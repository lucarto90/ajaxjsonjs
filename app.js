const btn1 = document.getElementById('boton1');
const btn2 = document.getElementById('boton2');

btn1.addEventListener('click', () => {
    const xhr = new XMLHttpRequest();

    xhr.open('GET', 'empleado.json', true);

    xhr.onload = function () {
        if (this.status === 200) {
            const persona = JSON.parse(this.responseText);

            const htmlTemplate = `
                <ul>
                    <li>ID: ${persona.id}</li>
                    <li>Nombre: ${persona.nombre}</li>
                    <li>Empresa: ${persona.empresa}</li>
                    <li>Trabajo: ${persona.trabajo}</li>
                </ul>
            `;

            document.getElementById('empleado').innerHTML = htmlTemplate;
        }
    }

    xhr.send();
});

btn2.addEventListener('click', () => {
    const xhr = new XMLHttpRequest();

    xhr.open('GET', 'empleados.json', true);

    xhr.onload = function () {
        if (this.status === 200) {
            const personal = JSON.parse(this.responseText);
            let htmlTemplate = '';

            personal.forEach(element => {
                htmlTemplate += `
                    <ul>
                        <li>ID: ${element.id}</li>
                        <li>Nombre: ${element.nombre}</li>
                        <li>Empresa: ${element.empresa}</li>
                        <li>Trabajo: ${element.trabajo}</li>
                    </ul>
                `;
            });
            

            document.getElementById('empleados').innerHTML = htmlTemplate;
        }
    }

    xhr.send()
});